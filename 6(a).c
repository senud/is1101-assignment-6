#include <stdio.h>

int numrows=1;

void TriangleP(int n){

	if(n>0){
		TriangleRow(numrows);
		numrows++;
		printf("\n");
		TriangleP(n-1);
	}
}

void TriangleRow(int y){

	if(y>0){
		printf("%d",y);
		TriangleRow(y-1);
	}
}

int main(int argc, char *argv[]) {

	int n;
	printf("Enter the number of the lines you want to print: ");
	scanf("%d", &n);

	TriangleP(n);

	return 0;
}

