#include <stdio.h>

void fibonacciSeq(int n){

	// initialize first and second terms
  int t1 = 0, t2 = 1;

	  // print the first two terms t1 and t2
  printf("\n%d \n ", t1);
  printf("\n%d \n ", t2);
  FindnxtTerm(n, t1, t2);

}


void FindnxtTerm(int n, int t1, int t2){

  int i=3;
	// initialize the next term (3rd term)
  int nextTerm = t1 + t2;


  if(i <= n+1) {

    printf("\n%d \n", nextTerm);
    t1 = t2;
    t2 = nextTerm;
    n--;
    FindnxtTerm(n, t1, t2);
  }

}


int main(int argc, char *argv[]) {

  int n;

  // get no. of terms from user
  printf("Enter the number of terms: ");
  scanf("%d", &n);
  fibonacciSeq(n);

	return 0;
}

